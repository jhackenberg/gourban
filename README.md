# GoUrban

GoUrban is a small Go library for fetching definitions from https://www.urbandictionary.com/.

It is limited in its functionality as there are only, at least to my knowledge, two api endpoints:
* `https://api.urbandictionary.com/v0/define` with `?term=SomeWord` or `?defid=SomeDefID`
* `https://api.urbandictionary.com/v0/random`

If you know of any further endpoints, feel free to create an issue or Merge Request.

## FAQ

Doesn't exist yet.

Feel free to extend this FAQ by creating a Merge Request or simply post your question as an issue.

If you have any further questions, please consider consulting this FAQ of another library (not mine) first:
https://github.com/NightfallAlicorn/urban-dictionary#faq.
